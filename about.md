---
title: About
layout: page
---

<!-- <img style="{width:100px;}" src="../assets/images/blog/new-logo/vdc-logo-v1.png" alt="vdc-logo" /> -->

<h1>Hello everyone!</h1>

<p>First of all, I would like to introduce myself! I am a very curious guy and have a lot of hobbies. Actually, I am interested in a lot of things! Passionate by computers and internet since I was a kid, I decided to do my studies in software engineering because it is a dream world for me. Why? Because I love creation/building. And programming is a so powerful tool to build. A simple laptop is enough to create everything you want! This is a world of innovations and non-stop evolution, I simply wanted to be part of this world.</p>

<p>To sum up the tech part, I have a good student background in Java and C#, acquired during my bachelor's degree in computer science. But for my first job, I decided to go into the MEAN stack universe (JS ES6, Angular, Mongo, NodeJS and Express...)! Then I moved to Blockchain technologies with still a lot of MEARN web development.</p>

<p>In order to learn more about my professional experiences, I invite you to consult my resume by clicking <a href="https://thomasvdc.com/assets/VANDERCAM_THOMAS_resume.pdf" target="_blank">here</a>.</p>

## References

- <a href="https://www.linkedin.com/in/justinberiot/" target="_blank">Justin Bériot</a> CTO at URBANTZ
- <a href="https://www.linkedin.com/in/aurelienlafolie/" target="_blank">Aurélien Lafolie</a> Co Managing Partner at Blue Chaingers
- <a href="https://www.linkedin.com/in/maxime-dekoninck-5a4baa9/" target="_blank">Maxime Dekoninck</a> Founder @ Cogarius
- <a href="https://www.linkedin.com/in/vaneeckhout/" target="_blank">Nicolas Van Eeckhout</a> Co-Founder/CEO @Cogarius
- <a href="https://www.linkedin.com/in/benjamin-m-596b74/" target="_blank">Benjamin MATEO</a> Blockchain Architect
- <a href="https://www.linkedin.com/in/stijn-brysbaert-2b634313/" target="_blank">Stijn Brysbaert</a> Project Manager at Rossel/Mediafin

## Availability

Currently available ✅

## Contact

- **Skype** - live:.cid.14efbb44de94a51c
- **Mail** - tvandercam@protonmail.com
- **LinkedIn** - <a target="_blank" href="https://www.linkedin.com/in/thomas-vdc/">thomas-vdc</a>

## More about me

<h4>Knowledge sharing</h4>

<img src="../assets/images/bd/blockchain-training.png" alt="training" />

<p>
Knowledge sharing has always been a big part of me. I naturally enjoy sharing what I learn with others, and I quickly discovered that this digital world was mostly filled with sharing, which is amazing.
Thanks to my last employer, I had the chance to create and animate some trainings. Thank you again for giving me this opportunity.
<p>

<h4>Hackathons</h4>

<img src="../assets/images/bd/thomashack.png" alt="hackathon" />

<p>
Being a startup during a weekend, meeting passionate people, and pushing the boundaries all together... Here are some good reasons to love these incredible events.
I participated in many Hackathons during my studies. It's amazing to see what we can build as a team on a weekend only. Having this incredibly short deadline generates many interesting situations where we have to quickly make some serious decisions.
</p>
