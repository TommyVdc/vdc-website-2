---
title: The brand new Remix IDE
---

As you already saw, Remix got a brand new user interface since v0.8.0. Sure, it might looks strange an a bit empty when your are used to the previous interface. But don't worry, we are going to do a quick overview of what has been changed.

## Plugin approach 

It is now easier to contribute to this fabulous tool. Indeed, you can now develop a plugin and easily plug and use it inside remix IDE. Similar to Visual Code's plugin module, it increases the tool's flexibility and allows more ande more new addons.

## Massive UI redesign

A new vision involve a new interface, a new user experience.

![remix-white-home](/assets/images/blog/remix2/remix-white-home.png)

First, the user land on a quite empty interface. It can frighten the first time, and you should ask yourself (how can I deploy my compile, how can I compile a contract ?!).
Actually, that's normal. You have first add each plugins to you IDE (don't panic, your plugins preferences are automatically saved for futures times).

But before all, let's switch to "Dark mode", because my eyes are hurting.

![remix-dark-mode](/assets/images/blog/remix2/remix-dark-mode.png)

Oh yeah thats so much better, isn't it?

Now, let's start building a Simple contract!

## Add your first plugins, essentials

First, let's plug a deployer and a compiler.

![remix-compiler](/assets/images/blog/remix2/remix-compiler.png)

![remix-compile-deploy](/assets/images/blog/remix2/remix-compile-deploy.png)

Your new plugins are now available from the toolbar on the left. We can now compile and deploy our first contract.

1. Create SimpleStorage.sol and fill it with this content:

```js
pragma solidity >=0.4.0 <0.7.0;

contract SimpleStorage {
    uint storedData;

    function set(uint x) public {
        storedData = x;
    }

    function get() public view returns (uint) {
        return storedData;
    }
}
```
[simple-storage](https://solidity.readthedocs.io/en/latest/introduction-to-smart-contracts.html)

2. Then, as always, compile and deploy it. Almost nothing changed for these steps.

![compile-and-deploy-it](/assets/images/blog/remix2/compile-deploy-it.png)

Back to the MAIN new feature, the plugin architecture and let's see how powerful it is.

Here is an example of interesting plugins also available from the plugin manager:
- Remix One Click Plugin: Generate a persistent interface for your smart contract directly from Remix.

The UI has been entirely redesigned. 

- Reeeeemiiiiiiiix ?
- What's new ? Highlights
===
v0.8.0-rc.1
@yann300 yann300 released this on May 20 · 612 commits to master since this release

This mark the first release candidate of 0.8.0:

integration of remix-plugin (https://github.com/ethereum/remix-plugin)
more infos https://remix.readthedocs.io

This is not yet published to NPM or remix.ethereum.org
:=====================

- Good to know / troubleshooting
	-  bytes32
	-  metamask