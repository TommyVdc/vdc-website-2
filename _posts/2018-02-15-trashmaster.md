---
title: "Trashmaster v0.0.0"
layout: post
date: 2018-02-15 23:10
tag: 
- web
- local
- project
image: ../assets/images/project/trashmaster/trash-glow-color.png
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
description: "Be the Trashmaster"
category: project
author: tommy
externalLink: false
---

![Desktop screenshot](../assets/images/project/trashmaster/trashmaster-web-screen.png "Trashmaster screenshot desktop")

# Throwback on Trashmaster (2015)

This is a very old project we had with 2 friends. The idea was to create a fun gamified application that incentivizes people at cleaning the streets.
The situation is critical at Charleroi, the city has this big unsolved problem of illegal dumping, and it's getting bigger each year. We really trust that this kind of application could have a huge positive impact.

The only reason why I wrote this article is just for sharing the idea, by hoping that maybe someone will be able to take the project as a student project, etc. Indeed, we unfortunately don't have the time to work properly on it anymore. A project requires time, not only on the tech part but also on the business part (finding partnerships, fundings, supports, etc).

So if you feel like an entrepreneur, love the idea and dreamed of a cleaner city, we have tons of resources to give you (for free obviously): 
- Business and technical analysis
- Source code
- Graphical resources
- Technical infrastructure (the server and domain are on me, I will pay them)
- Filled backlog with many ideas

If you have any questions, feel free the contact me (I do prefer mail or LinkedIn than article comments).

- [Trashmaster Website](https://trashmaster.be)
- [Trashmaster Meeting Demo App](https://app.trashmaster.be)

Have a great day!
