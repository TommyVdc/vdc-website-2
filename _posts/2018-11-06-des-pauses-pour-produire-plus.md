---
title: "#Lifestyle | Des pauses pour produire plus"
layout: post
category: blog
author: tommy
description: Astuces pour produire plus
date: '2018-11-06 22:53:15'
tag:
- break
- productivity
- pomodoro
---

![alt google break with a bike](../assets/images/blog/des-pauses/google-bike.jpg "google break with a bike")

Et si vous preniez une petite pause pour lire ce nouveau billet ?

Pour moi les pauses sont extrêmement importantes. On demande à un ingénieur d'être innovant, créatif et efficace, tout comme l'on demande à un sportif de savoir courir vite. Mais comme les muscles, le cerveau a besoin de se reposer pour pouvoir ensuite performer de plus belle. Un bodybuilder qui fait les bras tous les jours va régresser, ou pire, se blesser. Le cerveau lui ne se déchirera pas (heureusement), mais il sera perturbé d'une manière différente : fatigue importante, esprit embrouillé, perte de motivation. Et c'est normal !

Je suis personnellement quelqu'un qui prend beaucoup de pauses et j'avais envie de vous partager mon expérience ainsi que ma méthode de travail alliant repos et efficacité.

# L'avant et après Pomodoro

On m'a un jour fait découvrir une superbe application desktop : Pomodoro One (implémentant la méthode Pomodoro, forcément). Dès les premières utilisations, ce fut coup de foudre. Mais comment fonctionne Pomodoro ? Pour faire simple :

- Prenez une minuterie
- Définissez un temps de concentration (la méthode appelle ça un pomodoro). On conseille 20-25 minutes.
- Définissez un temps de pause (pour moi 5 mins)
- Let's go !

# Application

- Durant un pomodoro (période de concentration), faites tout pour rester focus, un peu comme pour la méditation. Sauf qu'ici, la pensée se focalise sur le traitement de la tâche. Pas de check SMS, SURTOUT PAS DE CHECK FACEBOOK, NON, FOCUS !
- Après 25 minutes, j'ai 5 mins de "repos". Dans mon cas, j'en profite pour prendre du recul sur mon code: "Est-ce que ce que je fais est toujours in-line avec ce qui a été pensé initialement ?", "Est-ce que le code que je m'apprête à écrire est cohérent, puis-je le récupérer d'un autre endroit ?" Parfois je me lève et vais en toucher deux mots à un collègue. Super important, j'en profite surtout pour fixer un point au loin durant 20 secondes: C'est un conseil très précieux qu'un ophtalmo m'a donné quand je lui ai dit que depuis que je travaillais derrière un écran, j'avais l'impression d'avoir eu une dégradation visuelle. Rapide et incomplète explication: nous avons deux types de vue; de loin et de près. Fixer un point au loin va soulager la vue de près (les muscles se détendent, etc). Si le sujet vous intéresse, allez demander à un ophtalmo ! Mais retenez juste que les yeux ont aussi besoin de se reposer :) 
- Pause terminée, on est reparti pour 25 minutes de focus ! 
- Tous les 4 pomodoros (dans mon cas, toutes les 2h), on prend une grande pause durant laquelle on se vide l'esprit en pensant à quelque chose de tout à fait différent. Allez prendre l'air, faites du sport sur le temps de midi, etc...). Changez d'environnement et faites quelqu'un chose qui vous plaît ! J'ai personnellement l'habitude de prendre un grand temps de midi pour faire une belle cassure entre la matinée et l'après-midi.

# Avantages de Pomodoro

- On apprend à mieux découper un problème en tâches (pour ensuite les caser dans un pomodoro). 
- On apprend à mieux estimer le temps de développement d'une tâche.
- On peut gamifier sa journée et se lancer de minis défis personnels (je dois coder ce truc en 25 minutes), j'adore ça !
- Nous force à soulager nos yeux.
- Augmentation de la capacité de concentration.
- Augmente la qualité de ce que l'on produit.
- Dans mon cas, me rend plus heureux.

# Le côté obscur de la pause
Les pauses Facebook !

Je pense que certains se reconnaîtront dans ce que je vais écrire, mais personnellement, j'ai l'impression que mon cerveau fond lorsque je vais sur Facebook. Je commence à flâner tout en scrollant, pour finir par me noyer dans une inondation de pubs camouflées (concours, fake buzzs, vidéos, etc...) ... j'en finis par me dire "Mais mer**, qu'est-ce que je fous là ?", pour finalement retourner à mes occupations, plus mous et fatigué qu'avant. Le pire, c'est que les yeux n'ont même pas pu se reposer ! 
La pause Facebook c'est un peu comme si un sprinter se reposait en trottinant derrière 3 pots d'échappement de bus très haut dans les tours. 
Facebook ou faire semblant de bosser est vraiment la pire des choses à faire. Ce n'est pas reposant, pas sain, pas agréable et au bout du compte forcément contre-productif !

# Conclusion

Il est possible de faire des pauses intelligentes et structurées qui vont avoir un gros impact positif sur plusieurs aspects de notre travail. C'est un petit peu comme durant notre sommeil, on a l'impression de ne rien faire et pourtant notre cerveau en profite pour trier et fixer le plus important. Si la pause est l'occasion de vider la cache, prendre du recul sur ce que l'on fait ou même discuter globalement du projet avec un collègue, elle peut alors être très productive !

Une bonne application Pomodoro est alors un allié précieux dans cette quête d'optimisation des performances et du bien-être au travail.
[Voici celle que j'utilise.](https://itunes.apple.com/be/app/tomato-one-free-focus-timer/id907364780?mt=12)

Merci à [Justin Bériot](https://be.linkedin.com/in/justinberiot) de m'avoir fait découvrir Pomodoro !