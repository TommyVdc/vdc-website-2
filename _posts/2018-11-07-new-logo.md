---
title: "#Design | Mon nouveau logo !"
layout: post
category: blog
author: tommy
tag:
- design
- logo
- sketch
date: '2018-11-07 00:00:00'
---

![alt new thomas blockchain logo](../assets/images/blog/new-logo/vdc-logo-v1.png "my new logo")

J'ai récemment décidé d'imprimer des cartes de visite et mon envie était de créer quelque chose d'unique qui me représentait. 

J'ai tout d'abord commencé sur papier par dessiner des formes, et rapidement le triangle m'est venu.

![alt new thomas blockchain logo](../assets/images/blog/new-logo/big-picture-vdc-logo-conception.png "my new logo")

J'y voyais un mixe entre T et V. Le tout sous forme de structure représentant la construction (c'est l'envie de bâtir qui m'a fait découvrir la programmation).
J'avais aussi envie d'incorporer la notion de Blockchain, technologie qui me fascine beaucoup et pour laquelle je m'investis pas mal depuis 1 an maintenant. J'ai donc décidé de relier les arêtes par des boules, symbolisant les noeuds d'une blockchain. C'est parfait, les arêtes de ma construction font déjà penser aux liens entre les noeuds !

Le tout coloré par un petit gradient de couleurs que j'affectionne.