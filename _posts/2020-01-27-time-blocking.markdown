---
title: "#Productivity | One life-changing month with Time Blocking"
layout: post
category: blog
tag:
- Productivity
- GameChanger
- TimeBlocking
author: tommy
date: '2020-01-25 07:40:29'
---

![watch](../assets/images/blog/time-blocking/tommy-watch-17h.jpg)

[My first productivity article]({% post_url 2018-11-06-des-pauses-pour-produire-plus %})

2020, my busiest year since I'm born. Indeed, this year, I am going to move to Vancouver BC for a big adventure. Right now I prepare all the stuff needed. Combined with my regular job, I actually have a lot of things to do. Because more than traveling, I'm going there for working purposes. It means a lot of things to build if I want to have a chance to get the best opportunities.
After a few times, I realized that I was a bit lost and overwhelmed in all these tasks to do during my busy time-off. So, I started looking for some productivity tips on Google. I tried and found some methodologies, and it definitely changed my life by making me able to accomplish all the tasks I had to do. All of that with confidence and the sensation that I have the control of my life. It was mind-blowing, I increased my productivity at an unexpected level.
This is why I'm here. To share with you my experience with these productivity tips I found and tuned for me. But don't forget to keep in mind one thing: we are all different. Indeed, we have different needs, sources of motivation, etc. Maybe these tips will not fit you. But at least you can try to tune them.

## Overview 

- [Overview](#overview)
- [Plan your day on a ToDo List the day before](#plan-your-day-on-a-todo-list-the-day-before)
- [Schedule your day / Time blocking](#schedule-your-day--time-blocking)
- [Feel free to use colors to have a better picture. I chose to order my tasks by category.](#feel-free-to-use-colors-to-have-a-better-picture-i-chose-to-order-my-tasks-by-category)
- [See your time as money](#see-your-time-as-money)
- [Gamification](#gamification)
- [Conclusion](#conclusion)
- [References](#references)

## Plan your day on a ToDo List the day before

This technique had been a game-changer for me. Each evening, I went to my desk, put some music, and started creating my tasks for the next day. This moment has to be relaxing. Take the time to see the big picture of your life's projects, and find which tasks have the biggest impact / has to be done first. At the same time, you can do an auto-evaluation of yourself and see if it's possible to close your daily tasks. It really is the best moment to questioning yourself about the way to reach your goals.

Some people are super fans of this way of planning the day because it offers a flexible way to organize your day.
In my specific case, it helps me a lot to increase my productivity. An important aspect is that I took the reflex to regularly check my Todo list. And the crazy thing is that it allowed me to stop wasting a part of my time. Each moment I got free time, I checked my Todo and took the next task, instead of chilling and realizing before sleeping that, one more time, I didn't progress well on my projects. Nevertheless, my overwhelming feeling was still there.
One day, I talked about this problem to a well-organized manager who had exactly the same issue. He talked to me about something called "Time Blocking".

## Schedule your day / Time blocking

![TimeBlockingCalendar](../assets/images/blog/time-blocking/agenda.png)

The trick here is to transpose the Todo method to the calendar approach. Keep planning the next day and question yourself in the evening, but schedule your tasks. Here are the main advantages of doing this:
- **Going deeper into your task estimations:** Your jobs are now on a timeline, you have to be realistic.
- **When it is time, it is time:** Your device reminds you that you have to do something right now, and not later. Sure it isn't very flexible, but personally, I need to keep this discipline. If you give me too much flexibility, I'm gonna see the task and say "Yeah, I can do it later". So in my case, it is more efficient. Between you and me, I sometimes cancel "events" when there is a really good reason 😅 So finally, it is not so rigid.
- **Seeing the big picture through the task history:** It's easier to see what you did today and when at a glance. Seeing all the tasks you did during the day/week can make you proud of what you did. And you don't have that feeling to did a lot of stuff but progressing on nothing. So yeah, be proud and use this energy to do more 😀 Virtuous circle!
- **Brainpower optimization with priorities:** Plan the most important tasks during your most productive time slots. And if you missed something today, you can reschedule in a smarter way tomorrow.
- **Never lose an information again:** Did you already say to friend whose sent you an article to read: "Oh thanks man, I'm gonna check it this evening!", and then never read it? Worst than never read it, forgot it! Now you have the solution: scheduling!
- **Searchable database:** If you efficiently name your tasks and use the note part to add some information, you can easily do a quick search to find them back. For instance, a colleague sent me 3 article links. As I've just told you, there are 90% chances that I never read them because "not the time today", and then you lose the links. So, I decided to create a specific event called "Read Justin's productivity articles", and then put all the links as event notes. If I need to find these articles one year later, a simple calendar search() will give them back to me. So easy!

## Feel free to use colors to have a better picture. I chose to order my tasks by category.

- Sport: Blue
- My company: Purple
- Friend: Red
- Client: Orange
- Task: Green

You are free to do whatever you want and use the color code you need. [Here is](https://larahogan.me/blog/manager-energy-drain/) a very interesting article about another way to order your tasks.

## See your time as money

We all know that it is important to save money. What about the time?
Time is a different unity, *every morning we wake up rich* with time, and then it is up to us to manage it. But because every morning we have another amount of time, most people don't care about wasting it. And that's ok, there is no perfect lifestyle. We all have to find what we want to do, and that's it. But if you are an entrepreneur, it can be very important to do smart time investments, as you invest your money. Because finally, the time you have to work on your projects will allow you to achieve them and realize your dreams. I am the CEO of my life, and I manage my time the better way to grow up! This is why Time Blocking makes sense to me.

> Highly inspired from [Ryan Serhant's video](https://www.youtube.com/watch?v=tsqkCYFxOb0).

## Gamification

As an entrepreneur, I love managing my life as a company. It makes my tasks nicer and easier to do. So, even if it looks very boring, stressful and energy-demanding for other people, I actually enjoy it and it makes me feel happier. It is like adding a color filter on my life, through him I see things differently. Some boring tasks can also become pleasant if you create a story around them.

![ColorFilter](../assets/images/blog/time-blocking/color-filter.png)

I also like challenging myself for finishing a task before the event's end: time trial!
Finally, each completed task(or success unlocked #Xbox360) makes you closer to your goal/dream. The calendar is a personal high score menu and you have to get things done to beat it!

## Conclusion

None solution can suit everyone, we are all different. In my opinion, a good approach is to test, tune and then see what happens. If *Todos* does not suit you, you can try *Time Blocking*, etc.
Both solutions can be tuned: feel free to use colors, bigger time slots instead of shorter if it makes you feel better. Finally, here is a non-exhaustive "summary" list of the benefits we talked about for these solutions.
- Increase your productivity
- Having more confidence
- Getting a big picture vision, your life's overview
- Questioning yourself
- Improving task time estimation
- Easily find back some information
- ...
And one last thing, just never forget that you are the boss of your life.

Thank you.

Special thanks to [Justin Bériot](https://be.linkedin.com/in/justinberiot) for sharing his *Time Blocking* experience with me.

## References

- https://medium.com/swlh/top-10-elon-musk-productivity-secrets-for-insane-success-dae584c88e03
- https://www.theladders.com/career-advice/how-to-manage-time-effectively-even-if-your-schedule-is-hectic
- https://blog.trello.com/balanced-productivity-tracker-with-trello
- https://curiosity.com/topics/elon-musk-and-bill-gates-schedule-their-days-in-5-minute-chunks-curiosity/
- https://www.youtube.com/watch?v=3X7t0GkYfxo
- https://larahogan.me/blog/manager-energy-drain/
- https://www.youtube.com/watch?v=tsqkCYFxOb0