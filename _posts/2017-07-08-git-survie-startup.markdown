---
title: "#Git | Kit de survie en startup"
layout: post
date: '2017-07-08 22:53:15'
tag:
- startup
- lifestyle
- git
category: blog
author: tommy
---

Pourquoi cet article ? Mon premier client en tant que consultant fut une startup ayant lancé un produit très complexe, développé par des gens hors du commun. Sortant à peine de l'école, je me suis immédiatement rendu compte que je connaissais en fait très mal git et que mes outils n’étaient pas top. J’ai donc eu envie de réaliser ce petit guide qui m’aurait super bien aidé à mes débuts.
Voyez plus cela comme un kit de survie vous donnant les outils nécessaires pour triompher au combat ! Evidemment, ce n'est qu'un début et surtout pas une liste exhaustive !
Assez de blabla, allons nous équiper !

## La base, ce bon vieux et indémodable terminal

Jetez votre client git à la poubelle ! Ici on ne joue qu’avec l’invit de commande ! Le premier que je vois sortir un egit, ungit, … me fera 50 pompes ! Maîtriser la base, c’est le plus important ;-) Sans cela, un client git peut rapidement se transformer en arme à double tranchant et détruire votre projet en réalisant des opérations cachées et invisibles pour vous.

*#Where is a Shell, there's a way. #The power of shell is in your hands !*

## Un dopant pour terminal : Oh My ZSH !

On va booster votre terminal pour en faire une véritable machine de guerre du dev !
De base, le prompt est vraiment pauvre. De base sur OSX, on ne voit même pas que l’on est sur un projet git, c'est horrible. 
Téléchargez donc [OhMyZSH](https://ohmyz.sh/). Il permet d’avoir toute une série d’infos importantes, comme la branche sur laquelle vous vous trouvez, le code de retour de la commande précédente, etc... Le tout avec de jolies couleurs pour bien s'y retrouver !

*Avant OhMyZSH*
![alt avant ohmyzsh](../assets/images/blog/git-pro-starter-pack/osx-terminal.png "avant ohmyzsh")

*Après OhMyZSH*
![alt après ohmyzsh](../assets/images/blog/git-pro-starter-pack/myzsh.png "après ohmyzsh")

La différence est flagrante !

## Un plugin git super pratique : COMMITIZEN

Des commits structurés sans effort ? C'est possible d'après [ce repo github](https://github.com/commitizen/cz-cli). Un bon commit permet de savoir ce qui a été modifié dans le code et quel est le secteur impacté par ces changements. Il est d’une aide précieuse pour assurer la stabilité de l’application et gagner du temps de débug. 

Après installation, tu pourras entrer `git cz`. Cela ouvrira un petit menu te guidant dans la rédaction de ton commit. Cela fait gagner du temps **et te force à toujours avoir un commit propre** :) Fini le push qui ne passe pas à cause d’une mauvaise syntaxe, car ici ce n'est plus toi qui écrit le squelette du commit !
Pour info, **git cz** se base sur la **convention de commit Angular**, si tu veux plus d’infos c’est par [ici](https://github.com/angular/angular.js/blob/master/CONTRIBUTING.md).  

Regarde moi ça !

![alt git cz menu](../assets/images/blog/git-pro-starter-pack/git-cz.png "git cz menu")

Sympa non ?

## Un petit coup de pouce de l’IDE !

Je vous ai dit plus haut qu’il fallait tout faire dans le terminal. Ouais mais... parfois on peut aussi s’aider de quelques petits outils pour réaliser les opérations simples et très souvent répétées, je cite; git add !

![alt git add vscode](../assets/images/blog/git-pro-starter-pack/git-add.gif "git add vscode")

## Avoir en tête les bonnes pratiques de gitflow

Un Gitflow est en quelque sorte une méthode de travail que l'équipe va suivre. Travailler de manière organisée afin de ne pas partir dans tous les sens et créer des problèmes.

Il a pour but d'avoir un historique le plus simple et lisible possible ! On appelle cet historique un arbre (c'est à ça qu'il ressemble lorsqu'on l'affiche -> `git log --graph —decorate —all`). Au revoir l'historique buisson qui part dans tous les sens !

Croyez-moi, avec un modèle buisson, on ne retrouve parfois jamais certains changements. Dans le cadre de la recherche d’un bug, avoir un bel arbre fait gagner énormément de temps et d’énergie. Avoir un arbre clean c'est avoir une app plus stable !

Le sujet est complexe, je reviendrai sur le workflow dans un autre poste dans lequel je parlerai du fameux rebase ! En attendant, voici un peu de lecture si la thématique vous intéresse: [a-successful-git-branching-model](http://nvie.com/posts/a-successful-git-branching-model/)

A bientôt, et que le push --force soit avec vous ! (lol)

Je tiens à remercier [Justin Bériot](https://be.linkedin.com/in/justinberiot) pour ses précieux conseils :)