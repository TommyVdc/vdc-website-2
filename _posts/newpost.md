---
title: New infra
---

Hi everyone, tonight, I just want to share with you this quick article about my new infrastructure.

Need
- Hosting my new website
- Hosting some new custom services under my domain thomas-vandercam.be (now thomasvdc.com)
- Having a huge flexibility

Because of that, I choosed to get a new VPS. But what does it stands for? Virtual Private Server. I do recommend to play with, cause if you are a passionate developer, it will teach you so many things. I got my first VPS during my first bachelor in 2013, cause I wanted to have an online entrypoint for different kind of services, like a git server. So I configured my own git server on myself, and I did a lot of errors haha, I before all, I leard A LOT.
So today, I'm gonna show you what did I do with my new VPS.

Ok first, wee need a cloud provider, I don't want to buy a entire server for myself, it's fucking expensive and I have no idea where I could store it °-__-°. So, way cheaper, I can ask to a cloud provider to rent a virtual part on their big servers, for only 3$/month. That seems fair. You can also get a free VPS during 1 year thanks to AWS. Go check the EC2 documentation if you are interested. But anyway, let's back to my VPS.
So I ordered a very small virtual machine (2Go RAM, 20Go SSD, Ubuntu 18.04), that's enough. Let's get work!

## Security

The server comes empty with Ubuntu and a ssh access to root account with an auto-generated password. We must quickly change that point haha.

### Keep your programs up to date

First update de package list, the most up-to-date they are, the less security breach will have ;)
Make sense, developers are fixing bugs everyday!
```sh
$ apt-get update
```
Then upgrade your installed packages by entering `apt-get upgrade`

### Change the default ssh port

Most robots try to brutforce you ssh on the default port 22. Changing this port can help a bit and reduce the attackers amount. Just check before that you new port is not already used yet. You can check the WELL KNOWN PORT NUMBERS from `/etc/services`.

Open `/etc/ssh/sshd_config` and look for `Port 22`. change it by the port you want, write and quit. Then restart the service `/etc/init.d/ssh restart`.

Don't forget to mention this new port for your next ssh connections `$ ssh root@IPVPS -p newPort`.

### Update the root password

```sh
$ passwd root
```

### Create a new account with limited access

It's too dangerous to always use the root. Too much permissions.

```sh
$ adduser newUserName
```

### Disable ssh connection with root 

Open this file `vim /etc/ssh/sshd_config`, then update that part:

```sh
# Authentication:

LoginGraceTime 2m
PermitRootLogin no # <------- Replace yes by no.
StrictModes yes
MaxAuthTries 4
```

Then restart the service `/etc/init.d/ssh restart`

### Install Fail2Ban

It is a very recommended protection against brutforce attacks. It will temporary block IPs which try to login after X wrong passwords in a certain range of time. But again, everything is customisable. Here is just the basics.

```sh
# Install Fail2Ban
$ apt-get install fail2ban

# You'll need to update the config file for some custom settings, so it is highly recommended to create a backup of your config file.
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.conf.backup
# The interesting part:
>

# Then update it
vim /etc/fail2ban/jail.conf
# And as always, restart the service ;) 
/etc/init.d/fail2ban restart
```

