Hello everyone, I hope you're fine today, cuz we are going to see something very interesting... Bitcoin transactions!!

Simple example, I have 1BTC and want to buy a pizza 0.7BTC. Let's do this!

As a user, I'm gonna take my Wallet (#What is a wallet), which is actually like a real wallet. I mean, imagine that there is really 1 BTC physically inside, like a real coin. I'm gonna get my wallet, take looking for the BTC, give it to the cashier, then the cashier is gonna to send me back 0.7BTC. 
It is exactly what happen when you transfer some BTC from your wallet to another one.

Go down one level...

utxo

input
coinbase (money creation)
output

locking script

WHY INPUT OUTPU?
Outputs neatly solve a few problems you would encounter with a balance based system.

Transactions using outputs can not be replayed cross network, multiple times, or in different situations than when they were created. In an address based system you must keep a nonce or a record of all past transactions to ensure multiple executions of one transaction does not occur. You don't have the option of re-signing a transaction with a higher miners fee, because the transactions are not atomic and can both exist in the chain at once.

Transactions using outputs can be deployed even years after they have been signed, which is incompatible with an incremented nonce.

An output based system can forget about an output once it is spent, a nonce based system must keep track of all nonces forever.

Verifying that an output exists in a block is trivial by displaying the merkle path to it, and showing the block header is part of the main chain. In an address based system this is substantially harder to prove, if not impossible given the same constraints.