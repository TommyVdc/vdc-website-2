---
title: "#Human | Visualiser l'invisible - Pollution de l'air"
layout: post
category: blog
tag:
- Luchtpijp
- air
- pollution
author: tommy
date: '2018-12-07 22:53:15'
---

# Visualiser l'invisible

Il y a peu, j'ai eu la chance de participer à un atelier de confection d'un capteur de particules fines. L'évent était organisé par **Luchtpijp**, avec le soutien de la Mutualité Chrétienne et d'autres groupes. Pour faire court, le projet de Luchtpijp est de vous fournir un **kit d'assemblage** simple, contenant le nécessaire pour monter un capteur artisanal. Celui-ci est ensuite placé et connecté chez vous pour s'ajouter à une map assez précise de la pollution de l'air en Belgique.

!["diy event"](../assets/images/blog/luchtpijp/diysensor.jpg "diy event")

## Comment ça marche ?

Et surtout de quoi avons-nous besoin pour construire un tel engin !? En fait, beaucoup de choses certes, mais grâce à internet, on peut trouver des pièces déjà très complètes !

Pièce maîtresse de notre création, le capteur de particules fines ! C'est grâce à son laser qu'il arrive à quantifier le nombre de particules. Plus exactement, de l'air est poussé dans une chambre traversée par un laser. A la rencontre d'une micro-particule, cette lumière s'irradie dans toutes les directions. Ensuite, un photomètre détecte cette lumière et est capable de calculer la concentration de particules dans la chambre à partir de cette dispersion.

![PMSensor](../assets/images/blog/luchtpijp/pmsensor.jpg "PMSensor")
*PM Sensor by [AirVisual](http://support.airvisual.com/knowledgebase/articles/1136839-how-do-the-pm2-5-sensor-and-co2-sensor-work)*

 Comme sur beaucoup d'autres projets, c'est le capteur **SDS011** qui a été choisi. Disponible sur AliExpress pour plus ou moins 17$, il reste très accessible. J'étais justement tombé sur un [article](https://hackernoon.com/how-to-measure-particulate-matter-with-a-raspberry-pi-75faa470ec35) avec une configuration très semblable.

Autant vous dire que c'est un **très bon choix** (le meilleur à mes yeux) car :

- Pas cher !
- Capable de détecter les PM10 et PM2.5
- Pas besoin de le calibrer, cela a déjà été fait convenablement en usine
- Ventilateur intégré
- Super populaire sur AliExpress
- Très bonne review d'[aqicn.org](http://aqicn.org/sensor/sds011/) qui l'a complètement démonté ! *"With its size, it is probably one of the best sensor in terms of accuracy"*
- ...

Concernant les autres composants, je ne préfère pas tout vous expliquer en détail. Voyez mon article comme un teaser. Je considère que cette oeuvre appartient à Luchtpijp et que si vous avez envie de vous en fabriquer un, la meilleure façon est de passer directement par eux en participant à un de leurs ateliers de confection en groupe.

Cela permet de :

- Soutenir un super mouvement 
- Passer un bon moment entre game-changers
- Manger un bon sandwich ;)
- Poser des questions

 Le tout pour **35 euros** ! C'est vraiment super correct et vous ne trouverez pas mieux seul. 

J'ai réalisé une simulation en achetant tout sur AliExpress: On peut gagner quelques euros (si pas de problème lol). Mais pour une telle réduc, on s'embête vraiment beaucoup pour les commandes, l'attente d'1 mois, on espère ne pas avoir de problèmes avec la douane... etc etc... Une belle perte de temps (et le temps c'est de l'argent) ! Donc, **[Achetez le chez Luchtpijp !!](https://www.growfunding.be/en/bxl/luchtpijp)** Vous y gagnerez :)

(si pas le temps pour l'atelier, vous pouvez quand même acheter un kit et le monter chez vous, c'est très bien expliqué dans le mode d'emploi fourni avec).

!["box"](../assets/images/blog/luchtpijp/box.jpg "box")

## Premiers résultats !

Une fois le détecteur branché et connecté à votre Wifi, il commence à envoyer des infos au serveur. Il est très simple d'avoir accès à ces informations via page dédiée.

![GraphPM2.5](../assets/images/blog/luchtpijp/graph-sensor-2.png "GraphPM2.5")
*Les graphs de quantité de particules PM10 et PM2.5 dans l'air*

Dans mon cas, les données matchent bien avec celles de l'application ["Brussels Air"](https://qualitedelair.brussels/ "Brussels Air") (app dispo sur Android et iOS via store officiel).

## Conclusion

C'est là une très belle initiative que Luchtpjd a lancé. Pour 35 euros, on peut réellement participer à un beau projet avec du bon matos ! ["La carte"](https://www.luchtpijp.be/kaart) générée à partir des données de chacun est vraiment impressionante et met le doigt sur ce problème de pollution.
Intéressé ? Ci-dessous, toutes les informations nécessaires pour vous aussi construire votre capteur de particules et montrer qu'il est temps pour nous de changer les choses :)

Crownfunding de Luchtpijp : https://www.growfunding.be/en/bxl/luchtpijp

Liens supplémentaires :

- https://forum.mysensors.org/topic/6404/next-generation-dust-sensor-for-mysensors
- https://www.facebook.com/luchtpijp/