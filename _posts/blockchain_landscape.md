Title: Do I need a Blockchain?
Description: Two years after the "crypto explosion", what's concretely happening today? Do we really need blockchains? Or, was it just another agnostic and holistic buzzword? Let's see together the current Blockchain state in Belgium and all around the world!
Language: FR/EN (depending on the audience)
Audience: Rated E for everyone

# Intro
Today's topic is pretty simple: "Do we need a blockchain?"
That's the question I'll ask to myself the first time that I hear something about a new "Blockchainized project". 

# Blockchain in a nutshell
A Blockchain is a storage place, simple is that. As much as a standard database actually. The difference is, the security. Blockchain system provides a higher security level. And as always, having something more, implies to do some trade-off... For instance, if I want hight performance engine with incredible acceleration, it will be more fragile. It's a question of compromises, related to our main use case.

# Security
The key success is the link between data. Thanks to this link, it become incredibly easy to detect that past data has been updated. It's like cutting a shoe lace. Once cut, it's impossible to strongly get them stuck together without a big bows at the middle that everyone can easily see. And that's the Blockchain power, every data cryptographically linked together. If you want to change something, you cut the link and everything is broken.

# Asking the good questions
Now, we're gonna adjust our needs regarding these questions:
1. Do I need advanced security on my data? 
2. What's my budget?
3. Do I need immutable data?
4. Do I need to decentralize my data? If yes, which degree of decentralization?
5. Do I need the highest security level?
6. Do I need transparent data / permissions?
7. Do I need to use the Mainnet?
8. Do I need a private and custom infrastructure?
9. Do I need an external service provider?

# Finding a suitable solution
Regarding the questions you answered above, here is a non-exhaustive list of different actual "blockchain" (in general) technologies:

1. *Do I need advanced security on my data?*
   * **YES** : Ok so it can be interesting to look for something you can use with your standard database.
   * **NO** : The standard database is a good option, don't worry ;)
2. *What's my budget?*
   * **I ain't any limits man** : Perfect, cuz some "blockchain" technologies come with a certain price :o
   * **I ain't no money** : Alright, it's possible to find something cheaper, even in the cryptosphere.
3. *Do I need immutable data?*
   * **YES** : Oooooh, so Blockchains and Distributed Ledger Technologies can fit your needs.
   * **NO** : Go add more securities to your standard database.
4. *Do I need to decentralize my data? If yes, which degree of decentralization?*
   * **YES, I need a hight decentralization degree** : Tezos 
5. *Do I need the highest security level?*
   * Tezos allows us to write more secured smart contracts thanks to technologies used in the nuclear sector, and so forth.
   * Ethereum is less secured but stays a strong choice.
   * Tron will bit faster than Ethereum, but on the other hand, you have to trade-off decentralization, which decrease the security level. Tron perfectly fit video games or UC that doesn't require to manage hight value assets.
6. *Do I need transparent data / permissions?*
   * Fabric (DLT) VS Ethereum (Blockchain) ...
7. *Do I need to use the Mainnet?*
8. *Do I need a private and custom infrastructure?*
9. *Do I need an external service provider?*

https://aws.amazon.com/fr/blockchain/